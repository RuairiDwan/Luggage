package ie.ucd.luggage;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Bomb bomb1 = new Bomb("big", 12);
		Laptop Lenovo = new Laptop("Touchscreen", 14);
		Pen bic = new Pen("blue", 1);
		DesignerPen ink = new DesignerPen("Red", 1, "Connect");
		
		//Constructor to create Suitcase
		Suitcase Black = new Suitcase(10);
		
		//Functions to add objects to list
		Black.add(ink);
		Black.add(bic);
		Black.add(bomb1);
		Black.add(Lenovo);
		
		//Function to get the weight of the bag
		Black.getBagWeight();

		//Constructor to create Safe Luggage
		SafeLuggage SafeBag = new SafeLuggage(20);
		
		//Constructor to add an object to the SafeLuggage list
		SafeBag.add(bomb1);
		System.out.println("The weight of the safe bag is: "+ SafeBag.getWeight());
		
		
		Suitcase Blue = new Suitcase(10);
		System.out.println(Blue.isDangerous());

		//Function to remove item from suitcase
		Black.removeItem(2);
		
		//Print statement to test if the object is dangerous 
		System.out.println(Black.isDangerous());
		
		//Print statements to test methods 
		System.out.println("Weight: " + bomb1.getWeight() + "Type:" + bomb1.getType() + "Dangerous:" + bomb1.isDangerous());
		System.out.println("Weight: " + Lenovo.getWeight() + "Type:" + Lenovo.getType() + "Dangerous:" + Lenovo.isDangerous());
		System.out.println("Weight: " + bic.getWeight() + "Type:" + bic.getType() + "Dangerous:" + bic.isDangerous());
		System.out.println("Weight: " + ink.getWeight() + "Type:" + ink.getType() + "Dangerous:" + ink.isDangerous());

	}
}
