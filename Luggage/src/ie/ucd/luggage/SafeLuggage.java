package ie.ucd.luggage;

import java.util.Scanner;

public class SafeLuggage extends Luggage {
	
	private String Password = "Password";
	private double bagWeight;

	public SafeLuggage(double bagWeight){
		this.bagWeight = bagWeight;
	}
	
	@Override
	public double getBagWeight() {
		// TODO Auto-generated method stub
		return bagWeight;
	}

	@Override
	public double getMaxWeight() {
		// TODO Auto-generated method stub
		return 50;
	}
	
	@Override
	public void add(Item item){
		System.out.println("Enter Password: ");
		Scanner scanner = new Scanner(System.in);
		String password = scanner.nextLine();
		scanner.close();
		System.out.println("Entered password is " + password);
		System.out.println("The password is " + Password);
		
		if (Password.equals(password)){
			System.out.println("Password was correct");
			super.add(item);
		}
		else{
			System.out.println("Password is incorrect");
		}
	}
	

}
