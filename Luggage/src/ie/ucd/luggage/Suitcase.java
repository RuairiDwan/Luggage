package ie.ucd.luggage;

public class Suitcase extends Luggage{

	private double bagWeight;
	
	public Suitcase(double bagWeight){
		this.bagWeight = bagWeight;
	}
	
	@Override
	public double getBagWeight() {
		// TODO Auto-generated method stub
		return bagWeight;
	}

	@Override
	public double getMaxWeight() {
		// TODO Auto-generated method stub
		return 50;
	}

}
