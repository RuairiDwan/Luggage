package ie.ucd.luggage;

public class Pen implements Item {
	public String type;
	private int weight;
	
	public Pen(String type, int weight){
		this.type = type;
		this.weight = weight;
	}
	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return type;
	}

	@Override
	public double getWeight() {
		// TODO Auto-generated method stub
		return weight;
	}

	@Override
	public boolean isDangerous() {
		// TODO Auto-generated method stub
		return false;
	}

}
